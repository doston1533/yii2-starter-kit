<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestQuestions */

$this->title = Yii::t('app', 'Create Test Questions');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-questions-create">

    <?= $this->render('_form', [
        'model' => $model,
        'models' => $models,
    ]) ?>

</div>
