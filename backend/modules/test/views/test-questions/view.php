<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\test\TestQuestions */
/* @var $models common\models\test\TestAnswers */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Questions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="test-questions-view">
    <?php if(!Yii::$app->request->isAjax){?>
    <div class="pull-right" style="margin-bottom: 15px;">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php // if (Yii::$app->user->can('test-questions/delete')): ?>
            <?php //  if ($model->status < $model::STATUS_SAVED): ?>
                <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                        'method' => 'post',
                    ],
                ]) ?>
            <?php // endif; ?>
        <?php // endif; ?>
        <?=  Html::a(Yii::t('app', 'Back'), ["index"], ['class' => 'btn btn-info']) ?>
    </div>
    <?php }?>
    <div class="card">
        <div class="card-header">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'id',
                    ],
                    [
                        'attribute' => 'question',
                    ],
                    [
                        'attribute' => 'type',
                        'value' => function($model) {
                            return $model->getTypeList($model['type']);
                        },
                    ],
                    [
                        'attribute' => 'category_id',
                        'value' => function($model) {
                            return $model->category->name;
                        },
                    ],
                    [
                        'attribute' => 'level_id',
                        'value' => function($model) {
                            return $model->level->name;
                        },
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function($model){
                            return (time()-$model->created_at<(60*60*24))?Yii::$app->formatter->format(date($model->created_at), 'relativeTime'):date('d.m.Y H:i',$model->created_at);
                        }
                    ],
                    [
                        'attribute' => 'updated_at',
                        'value' => function($model){
                            return (time()-$model->updated_at<(60*60*24))?Yii::$app->formatter->format(date($model->updated_at), 'relativeTime'):date('d.m.Y H:i',$model->updated_at);
                        }
                    ],
                    [
                        'attribute' => 'created_by',
                        'value' => function($model){
                            $username = \common\models\User::findOne($model->created_by)['username'];
                            return isset($username)?$username:$model->created_by;
                        }
                    ],
                    [
                        'attribute' => 'updated_by',
                        'value' => function($model){
                            $username = \common\models\User::findOne($model->updated_by)['username'];
                            return isset($username)?$username:$model->updated_by;
                        }
                    ],
                ],
            ]) ?>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Answer</th>
                        <th>Ball</th>
                        <th>Is true</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if(is_iterable($models)):
                        foreach ($models as $key => $value):
                    ?>
                    <tr>
                        <td><?= $key+1 ?></td>
                        <td><?= $value->id ?></td>
                        <td><?= $value->answer ?></td>
                        <td><?= $value->ball ?></td>
                        <td><?= $value->is_true ? '<i class="fa fa-check color-success"></i>' : '<i class="fa fa-times color-danger"></i>' ?></td>
                    </tr>
                    <?php
                        endforeach;
                    endif;
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
$css = <<< CSS
    .color-success{
        color: green;
    }
    .color-danger{
        color: red;
    }
CSS;
$this->registerCss($css);