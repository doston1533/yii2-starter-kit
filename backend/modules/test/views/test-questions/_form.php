<?php

use common\models\test\TestAnswers;
use unclead\multipleinput\components\BaseColumn;
use unclead\multipleinput\TabularColumn;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestQuestions */
/* @var $models common\models\test\TestAnswers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-questions-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class'=> 'customAjaxForm']]); ?>

    <div class="card">
        <div class="card-header">
            <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type')->dropDownList(\common\models\test\TestQuestions::getTypeList(), ['prompt'=>Yii::t('app', 'Select')]) ?>

            <?= $form->field($model, 'category_id')->dropDownList(\common\models\test\TestCategory::getList(true), ['prompt'=>Yii::t('app', 'Select')]) ?>

            <?= $form->field($model, 'level_id')->dropDownList(\common\models\test\TestLevel::getList(true), ['prompt'=>Yii::t('app', 'Select')]) ?>
        </div>
        <div class="card-body">
            <?= TabularInput::widget([
                'id' => 'test-answers',
                'models' => $models,
                'modelClass' => TestAnswers::class,
                'cloneButton' => true,
                'sortable' => true,
                'min' => 0,
                'addButtonPosition' => [
                    TabularInput::POS_HEADER,
                    TabularInput::POS_FOOTER,
                    TabularInput::POS_ROW
                ],
                'layoutConfig' => [
                    'offsetClass'   => 'col-sm-offset-4',
                    'labelClass'    => 'col-sm-2',
                    'wrapperClass'  => 'col-sm-10',
                    'errorClass'    => 'col-sm-4'
                ],
                'attributeOptions' => [
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnChange'       => false,
                    'validateOnSubmit'       => true,
                    'validateOnBlur'         => false,
                ],
                'form' => $form,
                'columns' => [
                    [
                        'name' => 'id',
                        'type' => BaseColumn::TYPE_HIDDEN_INPUT
                    ],
                    [
                        'name' => 'answer',
                        'title' => 'Title',
                        'type' => BaseColumn::TYPE_TEXT_INPUT,
                        'attributeOptions' => [
                            'enableClientValidation' => true,
                            'validateOnChange' => true,
                        ],
                        'enableError' => true
                    ],
                    [
                        'name' => 'ball',
                        'title' => 'Ball',
                        'type' => BaseColumn::TYPE_TEXT_INPUT,
                    ],
                    [
                        'name' => 'is_true',
                        'title' => 'Is True',
                        'type' => BaseColumn::TYPE_CHECKBOX,
                    ]
                ],
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
