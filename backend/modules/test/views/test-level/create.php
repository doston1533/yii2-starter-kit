<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestLevel */

$this->title = Yii::t('app', 'Create Test Level');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Levels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-level-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
