<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestCategory */

$this->title = Yii::t('app', 'Update Test Category: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="test-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
