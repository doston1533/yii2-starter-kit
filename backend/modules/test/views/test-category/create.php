<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestCategory */

$this->title = Yii::t('app', 'Create Test Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-category-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
