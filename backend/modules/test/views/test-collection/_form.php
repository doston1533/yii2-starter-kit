<?php

use unclead\multipleinput\components\BaseColumn;
use unclead\multipleinput\TabularInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestCollection */
/* @var $models common\models\test\TestCollectionQuestions */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-collection-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true, 'class'=> 'customAjaxForm']]); ?>

    <div class="card">
        <div class="card-head">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'category_id')->dropDownList(\common\models\test\TestCategory::getList(true), ['prompt'=>Yii::t('app', 'Select')]) ?>

            <?= $form->field($model, 'level_id')->dropDownList(\common\models\test\TestLevel::getList(true), ['prompt'=>Yii::t('app', 'Select')]) ?>

            <?= $form->field($model, 'time')->textInput() ?>
        </div>
        <div class="card-body">
            <?= TabularInput::widget([
                'id' => 'test-collection-questions',
                'models' => $models,
                'modelClass' => \common\models\test\TestCollectionQuestions::class,
                'cloneButton' => true,
                'sortable' => true,
                'min' => 0,
                'addButtonPosition' => [
                    TabularInput::POS_HEADER,
                    TabularInput::POS_FOOTER,
                    TabularInput::POS_ROW
                ],
                'layoutConfig' => [
                    'offsetClass'   => 'col-sm-offset-4',
                    'labelClass'    => 'col-sm-2',
                    'wrapperClass'  => 'col-sm-10',
                    'errorClass'    => 'col-sm-4'
                ],
                'attributeOptions' => [
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnChange'       => false,
                    'validateOnSubmit'       => true,
                    'validateOnBlur'         => false,
                ],
                'form' => $form,
                'columns' => [
                    [
                        'name' => 'id',
                        'type' => BaseColumn::TYPE_HIDDEN_INPUT
                    ],
                    [
                        'name' => 'question_id',
                        'title' => 'Question',
                        'type' => \kartik\select2\Select2::class,
                        'options' => [
                            'data' => \common\models\test\TestQuestions::getList(true, 'id', 'question'),
                            'options' => [
                                'prompt' => Yii::t('app', 'Select'),
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'attributeOptions' => [
                            'enableClientValidation' => true,
                            'validateOnChange' => true,
                        ],
                        'enableError' => true
                    ],
                    [
                        'name' => 'time',
                        'title' => 'Time',
                        'type' => BaseColumn::TYPE_TEXT_INPUT,
                        'attributeOptions' => [
                            'enableClientValidation' => true,
                            'validateOnChange' => true,
                        ],
                        'enableError' => true,
                        'options' => [
                            'type' => 'number',
                        ],
                    ],
                ],
            ]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
