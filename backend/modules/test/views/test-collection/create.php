<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\test\TestCollection */

$this->title = Yii::t('app', 'Create Test Collection');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Test Collections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-collection-create">

    <?= $this->render('_form', [
        'model' => $model,
        'models' => $models,
    ]) ?>

</div>
