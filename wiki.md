

php console/yii migrate/create create_test_category_table -f="name:string(255):notNull:unique,status:smallInteger(6):defaultValue(1),created_at:integer(11),updated_at:integer(11),created_by:integer(11),updated_by:integer(11)"

php console/yii migrate/create create_test_level_table -f="name:string(255):notNull:unique,status:smallInteger(6):defaultValue(1),created_at:integer(11),updated_at:integer(11),created_by:integer(11),updated_by:integer(11)"

php console/yii migrate/create create_test_questions_table -f="question:string(255):notNull:unique, type:smallInteger(6):defaultValue(1), category_id:integer:foreignKey(test_category), level_id:integer:foreignKey(test_level), status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"

php console/yii migrate/create create_test_answers_table -f="answer:string(255):notNull:unique, question_id:integer:foreignKey(test_questions), is_true:boolean:defaultValue(false), type:smallInteger:defaultValue(1), ball:float, status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"

php console/yii migrate/create create_test_collection_table -f="name:string(255):notNull:unique, category_id:integer:foreignKey(test_category), level_id:integer:foreignKey(test_level), time:float, status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"

php console/yii migrate/create create_test_collection_questions_table -f="collection_id:integer:foreignKey(test_collection), question_id:integer:foreignKey(test_questions), time:float, status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"

php console/yii migrate/create create_test_session_table -f="user_id:integer:foreignKey(user), collection_id:integer:foreignKey(test_collection), time_start:integer(11), time_end:integer(11), status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"

php console/yii migrate/create create_test_session_answers_table -f="session_id:integer:foreignKey(test_session), user_id:integer:foreignKey(user), question_id:integer:foreignKey(test_questions), answer_id:integer:foreignKey(test_answers), is_correct:boolean:defaultValue(false), time:float, time_start:integer(11), time_end:integer(11), ball:float, status:smallInteger(6):defaultValue(1), created_at:integer(11), updated_at:integer(11), created_by:integer(11), updated_by:integer(11)"