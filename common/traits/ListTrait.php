<?php

namespace common\traits;

trait ListTrait
{
    private static $list = [];

    public static function getList(bool $map = false, $from = null, $to = null)
    {
        if (empty(self::$list[static::class])) {
            self::$list[static::class] = static::find()->select([$from ?? 'id', $to ?? 'name'])->asArray()->all();
        }
        if ($map) {
            return array_column(self::$list[static::class], $to ?? 'name', $from ?? 'id');
        }
        return self::$list[static::class];
    }
}