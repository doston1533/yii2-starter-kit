<?php

namespace common\models\query;

use common\models\BaseModel;
use yii\db\ActiveQuery;

/**
 * Class BaseQuery
 * @package common\models\query
 * @author Eugene Terentev <eugene@terentev.net>
 */
class BaseQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function notDeleted()
    {
        $this->andWhere(['!=', 'status', BaseModel::STATUS_DELETED]);
        return $this;
    }

    /**
     * @return $this
     */
    public function active()
    {
        $this->andWhere(['status' => BaseModel::STATUS_ACTIVE]);
        return $this;
    }
}