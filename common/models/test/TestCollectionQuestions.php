<?php

namespace common\models\test;

use common\models\BaseModel;
use Yii;

/**
 * This is the model class for table "{{%test_collection_questions}}".
 *
 * @property int $id
 * @property int|null $collection_id
 * @property int|null $question_id
 * @property float|null $time
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestCollection $collection
 * @property TestQuestions $question
 */
class TestCollectionQuestions extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_collection_questions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['collection_id', 'question_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['time'], 'number'],
            [['collection_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestCollection::class, 'targetAttribute' => ['collection_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestQuestions::class, 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'time' => Yii::t('app', 'Time'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Collection]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(TestCollection::class, ['id' => 'collection_id']);
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TestQuestions::class, ['id' => 'question_id']);
    }
}
