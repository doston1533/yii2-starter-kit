<?php

namespace common\models\test;

use common\models\BaseModel;
use common\traits\ListTrait;
use Yii;

/**
 * This is the model class for table "{{%test_questions}}".
 *
 * @property int $id
 * @property string $question
 * @property int|null $type
 * @property int|null $category_id
 * @property int|null $level_id
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestCategory $category
 * @property TestLevel $level
 * @property TestAnswers[] $testAnswers
 * @property TestCollectionQuestions[] $testCollectionQuestions
 * @property TestSessionAnswers[] $testSessionAnswers
 */
class TestQuestions extends BaseModel
{
    use ListTrait;
    public const TYPE_OPTION = 1;
    public const TYPE_ANSWER = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_questions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question'], 'required'],
            [['type', 'category_id', 'level_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['question'], 'string', 'max' => 255],
            [['question'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestLevel::class, 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'type' => Yii::t('app', 'Type'),
            'category_id' => Yii::t('app', 'Category ID'),
            'level_id' => Yii::t('app', 'Level ID'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TestCategory::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Level]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(TestLevel::class, ['id' => 'level_id']);
    }

    /**
     * Gets query for [[TestAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestAnswers()
    {
        return $this->hasMany(TestAnswers::class, ['question_id' => 'id']);
    }

    /**
     * Gets query for [[TestCollectionQuestions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestCollectionQuestions()
    {
        return $this->hasMany(TestCollectionQuestions::class, ['question_id' => 'id']);
    }

    /**
     * Gets query for [[TestSessionAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestSessionAnswers()
    {
        return $this->hasMany(TestSessionAnswers::class, ['question_id' => 'id']);
    }

    public static function getTypeList($key = null)
    {
        $list = [
            self::TYPE_OPTION => Yii::t('app', 'Option'),
            self::TYPE_ANSWER => Yii::t('app', 'Answer'),
        ];
        return $key !== null ? $list[$key] : $list;
    }

    public function getAnswerCount()
    {
        return $this->getTestAnswers()->count();
    }
}
