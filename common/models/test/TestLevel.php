<?php

namespace common\models\test;

use common\models\BaseModel;
use common\traits\ListTrait;
use Yii;

/**
 * This is the model class for table "{{%test_level}}".
 *
 * @property int $id
 * @property string $name
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestCollection[] $testCollections
 * @property TestQuestions[] $testQuestions
 */
class TestLevel extends BaseModel
{
    use ListTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_level}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[TestCollections]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestCollections()
    {
        return $this->hasMany(TestCollection::class, ['level_id' => 'id']);
    }

    /**
     * Gets query for [[TestQuestions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestQuestions()
    {
        return $this->hasMany(TestQuestions::class, ['level_id' => 'id']);
    }
}
