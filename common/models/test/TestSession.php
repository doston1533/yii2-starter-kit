<?php

namespace common\models\test;

use common\models\BaseModel;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%test_session}}".
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $collection_id
 * @property int|null $time_start
 * @property int|null $time_end
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestCollection $collection
 * @property TestSessionAnswers[] $testSessionAnswers
 * @property User $user
 */
class TestSession extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_session}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'collection_id', 'time_start', 'time_end', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['collection_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestCollection::class, 'targetAttribute' => ['collection_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Collection]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(TestCollection::class, ['id' => 'collection_id']);
    }

    /**
     * Gets query for [[TestSessionAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestSessionAnswers()
    {
        return $this->hasMany(TestSessionAnswers::class, ['session_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
