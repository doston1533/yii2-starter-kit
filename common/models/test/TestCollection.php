<?php

namespace common\models\test;

use common\models\BaseModel;
use common\traits\ListTrait;
use Yii;

/**
 * This is the model class for table "{{%test_collection}}".
 *
 * @property int $id
 * @property string $name
 * @property int|null $category_id
 * @property int|null $level_id
 * @property float|null $time
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestCategory $category
 * @property TestLevel $level
 * @property TestCollectionQuestions[] $testCollectionQuestions
 * @property TestSession[] $testSessions
 */
class TestCollection extends BaseModel
{
    use ListTrait;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_collection}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['category_id', 'level_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['time'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestCategory::class, 'targetAttribute' => ['category_id' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestLevel::class, 'targetAttribute' => ['level_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'category_id' => Yii::t('app', 'Category ID'),
            'level_id' => Yii::t('app', 'Level ID'),
            'time' => Yii::t('app', 'Time'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(TestCategory::class, ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Level]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(TestLevel::class, ['id' => 'level_id']);
    }

    /**
     * Gets query for [[TestCollectionQuestions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestCollectionQuestions()
    {
        return $this->hasMany(TestCollectionQuestions::class, ['collection_id' => 'id']);
    }

    /**
     * Gets query for [[TestSessions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestSessions()
    {
        return $this->hasMany(TestSession::class, ['collection_id' => 'id']);
    }
}
