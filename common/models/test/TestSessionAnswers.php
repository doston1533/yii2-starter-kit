<?php

namespace common\models\test;

use common\models\BaseModel;
use common\models\User;
use Yii;

/**
 * This is the model class for table "{{%test_session_answers}}".
 *
 * @property int $id
 * @property int|null $session_id
 * @property int|null $user_id
 * @property int|null $question_id
 * @property int|null $answer_id
 * @property int|null $is_correct
 * @property float|null $time
 * @property int|null $time_start
 * @property int|null $time_end
 * @property float|null $ball
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestAnswers $answer
 * @property TestQuestions $question
 * @property TestSession $session
 * @property User $user
 */
class TestSessionAnswers extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_session_answers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['session_id', 'user_id', 'question_id', 'answer_id', 'is_correct', 'time_start', 'time_end', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['time', 'ball'], 'number'],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestAnswers::class, 'targetAttribute' => ['answer_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestQuestions::class, 'targetAttribute' => ['question_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestSession::class, 'targetAttribute' => ['session_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'question_id' => Yii::t('app', 'Question ID'),
            'answer_id' => Yii::t('app', 'Answer ID'),
            'is_correct' => Yii::t('app', 'Is Correct'),
            'time' => Yii::t('app', 'Time'),
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'ball' => Yii::t('app', 'Ball'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Answer]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAnswer()
    {
        return $this->hasOne(TestAnswers::class, ['id' => 'answer_id']);
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TestQuestions::class, ['id' => 'question_id']);
    }

    /**
     * Gets query for [[Session]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(TestSession::class, ['id' => 'session_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
