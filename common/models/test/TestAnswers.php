<?php

namespace common\models\test;

use common\models\BaseModel;
use Yii;

/**
 * This is the model class for table "{{%test_answers}}".
 *
 * @property int $id
 * @property string $answer
 * @property int|null $question_id
 * @property int|null $is_true
 * @property int|null $type
 * @property float|null $ball
 * @property int|null $status
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property TestQuestions $question
 * @property TestSessionAnswers[] $testSessionAnswers
 */
class TestAnswers extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%test_answers}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['answer'], 'required'],
            [['question_id', 'is_true', 'type', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['ball'], 'number'],
            [['answer'], 'string', 'max' => 255],
            [['answer'], 'unique'],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => TestQuestions::class, 'targetAttribute' => ['question_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'answer' => Yii::t('app', 'Answer'),
            'question_id' => Yii::t('app', 'Question ID'),
            'is_true' => Yii::t('app', 'Is True'),
            'type' => Yii::t('app', 'Type'),
            'ball' => Yii::t('app', 'Ball'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * Gets query for [[Question]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(TestQuestions::class, ['id' => 'question_id']);
    }

    /**
     * Gets query for [[TestSessionAnswers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTestSessionAnswers()
    {
        return $this->hasMany(TestSessionAnswers::class, ['answer_id' => 'id']);
    }
}
