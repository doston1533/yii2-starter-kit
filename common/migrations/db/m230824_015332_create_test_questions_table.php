<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_questions}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test_category}}`
 * - `{{%test_level}}`
 */
class m230824_015332_create_test_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_questions}}', [
            'id' => $this->primaryKey(),
            'question' => $this->string(255)->notNull()->unique(),
            'type' => $this->smallInteger(6)->defaultValue(1),
            'category_id' => $this->integer(),
            'level_id' => $this->integer(),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-test_questions-category_id}}',
            '{{%test_questions}}',
            'category_id'
        );

        // add foreign key for table `{{%test_category}}`
        $this->addForeignKey(
            '{{%fk-test_questions-category_id}}',
            '{{%test_questions}}',
            'category_id',
            '{{%test_category}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `level_id`
        $this->createIndex(
            '{{%idx-test_questions-level_id}}',
            '{{%test_questions}}',
            'level_id'
        );

        // add foreign key for table `{{%test_level}}`
        $this->addForeignKey(
            '{{%fk-test_questions-level_id}}',
            '{{%test_questions}}',
            'level_id',
            '{{%test_level}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test_category}}`
        $this->dropForeignKey(
            '{{%fk-test_questions-category_id}}',
            '{{%test_questions}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-test_questions-category_id}}',
            '{{%test_questions}}'
        );

        // drops foreign key for table `{{%test_level}}`
        $this->dropForeignKey(
            '{{%fk-test_questions-level_id}}',
            '{{%test_questions}}'
        );

        // drops index for column `level_id`
        $this->dropIndex(
            '{{%idx-test_questions-level_id}}',
            '{{%test_questions}}'
        );

        $this->dropTable('{{%test_questions}}');
    }
}
