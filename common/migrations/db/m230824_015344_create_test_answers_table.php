<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_answers}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%questions}}`
 */
class m230824_015344_create_test_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_answers}}', [
            'id' => $this->primaryKey(),
            'answer' => $this->string(255)->notNull()->unique(),
            'question_id' => $this->integer(),
            'is_true' => $this->boolean()->defaultValue(false),
            'type' => $this->smallInteger()->defaultValue(1),
            'ball' => $this->float()->defaultValue(0),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `question_id`
        $this->createIndex(
            '{{%idx-test_answers-question_id}}',
            '{{%test_answers}}',
            'question_id'
        );

        // add foreign key for table `{{%questions}}`
        $this->addForeignKey(
            '{{%fk-test_answers-question_id}}',
            '{{%test_answers}}',
            'question_id',
            '{{%test_questions}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%questions}}`
        $this->dropForeignKey(
            '{{%fk-test_answers-question_id}}',
            '{{%test_answers}}'
        );

        // drops index for column `question_id`
        $this->dropIndex(
            '{{%idx-test_answers-question_id}}',
            '{{%test_answers}}'
        );

        $this->dropTable('{{%test_answers}}');
    }
}
