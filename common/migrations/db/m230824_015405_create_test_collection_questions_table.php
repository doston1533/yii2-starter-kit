<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_collection_questions}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test_collection}}`
 * - `{{%test_questions}}`
 */
class m230824_015405_create_test_collection_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_collection_questions}}', [
            'id' => $this->primaryKey(),
            'collection_id' => $this->integer(),
            'question_id' => $this->integer(),
            'time' => $this->float(),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `collection_id`
        $this->createIndex(
            '{{%idx-test_collection_questions-collection_id}}',
            '{{%test_collection_questions}}',
            'collection_id'
        );

        // add foreign key for table `{{%test_collection}}`
        $this->addForeignKey(
            '{{%fk-test_collection_questions-collection_id}}',
            '{{%test_collection_questions}}',
            'collection_id',
            '{{%test_collection}}',
            'id',
            'CASCADE'
        );

        // creates index for column `question_id`
        $this->createIndex(
            '{{%idx-test_collection_questions-question_id}}',
            '{{%test_collection_questions}}',
            'question_id'
        );

        // add foreign key for table `{{%test_questions}}`
        $this->addForeignKey(
            '{{%fk-test_collection_questions-question_id}}',
            '{{%test_collection_questions}}',
            'question_id',
            '{{%test_questions}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test_collection}}`
        $this->dropForeignKey(
            '{{%fk-test_collection_questions-collection_id}}',
            '{{%test_collection_questions}}'
        );

        // drops index for column `collection_id`
        $this->dropIndex(
            '{{%idx-test_collection_questions-collection_id}}',
            '{{%test_collection_questions}}'
        );

        // drops foreign key for table `{{%test_questions}}`
        $this->dropForeignKey(
            '{{%fk-test_collection_questions-question_id}}',
            '{{%test_collection_questions}}'
        );

        // drops index for column `question_id`
        $this->dropIndex(
            '{{%idx-test_collection_questions-question_id}}',
            '{{%test_collection_questions}}'
        );

        $this->dropTable('{{%test_collection_questions}}');
    }
}
