<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_session_answers}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test_session}}`
 * - `{{%user}}`
 * - `{{%test_questions}}`
 * - `{{%test_answers}}`
 */
class m230824_015511_create_test_session_answers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_session_answers}}', [
            'id' => $this->primaryKey(),
            'session_id' => $this->integer(),
            'user_id' => $this->integer(),
            'question_id' => $this->integer(),
            'answer_id' => $this->integer(),
            'is_correct' => $this->boolean()->defaultValue(false),
            'time' => $this->float(),
            'time_start' => $this->integer(11),
            'time_end' => $this->integer(11),
            'ball' => $this->float(),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `session_id`
        $this->createIndex(
            '{{%idx-test_session_answers-session_id}}',
            '{{%test_session_answers}}',
            'session_id'
        );

        // add foreign key for table `{{%test_session}}`
        $this->addForeignKey(
            '{{%fk-test_session_answers-session_id}}',
            '{{%test_session_answers}}',
            'session_id',
            '{{%test_session}}',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-test_session_answers-user_id}}',
            '{{%test_session_answers}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-test_session_answers-user_id}}',
            '{{%test_session_answers}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `question_id`
        $this->createIndex(
            '{{%idx-test_session_answers-question_id}}',
            '{{%test_session_answers}}',
            'question_id'
        );

        // add foreign key for table `{{%test_questions}}`
        $this->addForeignKey(
            '{{%fk-test_session_answers-question_id}}',
            '{{%test_session_answers}}',
            'question_id',
            '{{%test_questions}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `answer_id`
        $this->createIndex(
            '{{%idx-test_session_answers-answer_id}}',
            '{{%test_session_answers}}',
            'answer_id'
        );

        // add foreign key for table `{{%test_answers}}`
        $this->addForeignKey(
            '{{%fk-test_session_answers-answer_id}}',
            '{{%test_session_answers}}',
            'answer_id',
            '{{%test_answers}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test_session}}`
        $this->dropForeignKey(
            '{{%fk-test_session_answers-session_id}}',
            '{{%test_session_answers}}'
        );

        // drops index for column `session_id`
        $this->dropIndex(
            '{{%idx-test_session_answers-session_id}}',
            '{{%test_session_answers}}'
        );

        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-test_session_answers-user_id}}',
            '{{%test_session_answers}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-test_session_answers-user_id}}',
            '{{%test_session_answers}}'
        );

        // drops foreign key for table `{{%test_questions}}`
        $this->dropForeignKey(
            '{{%fk-test_session_answers-question_id}}',
            '{{%test_session_answers}}'
        );

        // drops index for column `question_id`
        $this->dropIndex(
            '{{%idx-test_session_answers-question_id}}',
            '{{%test_session_answers}}'
        );

        // drops foreign key for table `{{%test_answers}}`
        $this->dropForeignKey(
            '{{%fk-test_session_answers-answer_id}}',
            '{{%test_session_answers}}'
        );

        // drops index for column `answer_id`
        $this->dropIndex(
            '{{%idx-test_session_answers-answer_id}}',
            '{{%test_session_answers}}'
        );

        $this->dropTable('{{%test_session_answers}}');
    }
}
