<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_session}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%user}}`
 * - `{{%test_collection}}`
 */
class m230824_015415_create_test_session_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_session}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'collection_id' => $this->integer(),
            'time_start' => $this->integer(11),
            'time_end' => $this->integer(11),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-test_session-user_id}}',
            '{{%test_session}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-test_session-user_id}}',
            '{{%test_session}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `collection_id`
        $this->createIndex(
            '{{%idx-test_session-collection_id}}',
            '{{%test_session}}',
            'collection_id'
        );

        // add foreign key for table `{{%test_collection}}`
        $this->addForeignKey(
            '{{%fk-test_session-collection_id}}',
            '{{%test_session}}',
            'collection_id',
            '{{%test_collection}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-test_session-user_id}}',
            '{{%test_session}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-test_session-user_id}}',
            '{{%test_session}}'
        );

        // drops foreign key for table `{{%test_collection}}`
        $this->dropForeignKey(
            '{{%fk-test_session-collection_id}}',
            '{{%test_session}}'
        );

        // drops index for column `collection_id`
        $this->dropIndex(
            '{{%idx-test_session-collection_id}}',
            '{{%test_session}}'
        );

        $this->dropTable('{{%test_session}}');
    }
}
