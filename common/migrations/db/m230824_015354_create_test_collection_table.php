<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_collection}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%test_category}}`
 * - `{{%test_level}}`
 */
class m230824_015354_create_test_collection_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_collection}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull()->unique(),
            'category_id' => $this->integer(),
            'level_id' => $this->integer(),
            'time' => $this->float(),
            'status' => $this->smallInteger(6)->defaultValue(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ]);

        // creates index for column `category_id`
        $this->createIndex(
            '{{%idx-test_collection-category_id}}',
            '{{%test_collection}}',
            'category_id'
        );

        // add foreign key for table `{{%test_category}}`
        $this->addForeignKey(
            '{{%fk-test_collection-category_id}}',
            '{{%test_collection}}',
            'category_id',
            '{{%test_category}}',
            'id',
            'RESTRICT'
        );

        // creates index for column `level_id`
        $this->createIndex(
            '{{%idx-test_collection-level_id}}',
            '{{%test_collection}}',
            'level_id'
        );

        // add foreign key for table `{{%test_level}}`
        $this->addForeignKey(
            '{{%fk-test_collection-level_id}}',
            '{{%test_collection}}',
            'level_id',
            '{{%test_level}}',
            'id',
            'RESTRICT'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%test_category}}`
        $this->dropForeignKey(
            '{{%fk-test_collection-category_id}}',
            '{{%test_collection}}'
        );

        // drops index for column `category_id`
        $this->dropIndex(
            '{{%idx-test_collection-category_id}}',
            '{{%test_collection}}'
        );

        // drops foreign key for table `{{%test_level}}`
        $this->dropForeignKey(
            '{{%fk-test_collection-level_id}}',
            '{{%test_collection}}'
        );

        // drops index for column `level_id`
        $this->dropIndex(
            '{{%idx-test_collection-level_id}}',
            '{{%test_collection}}'
        );

        $this->dropTable('{{%test_collection}}');
    }
}
